﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI_SaleCoKhi.Models
{
    public class ResultModel
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
        public static ResultModel MakeError(string message)
        {
            return new ResultModel
            {
                Success = false,
                Message = message,
            };
        }
        public static ResultModel MakeSucess(string message)
        {
            return new ResultModel
            {
                Success = true,
                Message = message,
            };
        }

        public static ResultModel MakeSucess(string message,int id)
        {
            return new ResultModel
            {
                Id = id,
                Success = true,
                Message = message,
            };
        }
    }
}