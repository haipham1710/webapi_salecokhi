﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace WebAPI_SaleCoKhi.Controllers
{
    public class UserController : ApiController
    {


        [HttpGet]
        public IEnumerable<NhanVienVM> GettbNhanVienLists()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbNhanViens.Select(x => new NhanVienVM
            {
                Id = x.id,
                account = x.TenDangNhap,
                userName = x.TenNhanVien,
                password = x.MatKhau,
                position=x.ChucVu,
                phone = x.SoDienThoai,
                invalid = x.HopLe,
            }).AsEnumerable();
        }
        [HttpGet]
        public IEnumerable<NhanVienVM> GettbNhanVienLists(string keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbNhanViens.Where(p=>p.TenNhanVienString.Contains(keyword)).Select(x => new NhanVienVM
            {
                Id = x.id,
                account = x.TenDangNhap,
                userName = x.TenNhanVien,
                password = x.MatKhau,
                position = x.ChucVu,
                phone = x.SoDienThoai,
                invalid = x.HopLe,
            }).AsEnumerable();
        }
        [HttpGet]
        public JObject login(string username,string password)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            NhanVienVM nv = db.tbNhanViens.Where(p => p.TenDangNhap.Equals(username)).Select(x => new NhanVienVM
            {
                Id = x.id,
                account = x.TenDangNhap,
                userName = x.TenNhanVien,
                password = x.MatKhau,
                position = x.ChucVu,
                phone = x.SoDienThoai,
                invalid = x.HopLe,
            }).FirstOrDefault();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if(nv !=null)
            {
                if(nv.password.Equals(password))
                {
                    if(nv.invalid == true)
                        return JObject.FromObject((new Msg { msg = "Success", idNv = (int)nv.Id }));
                    else
                        return JObject.FromObject((new Msg { msg = "Tài khoản đã bị khóa", idNv = 0 }));
                }
                else
                    return JObject.FromObject((new Msg { msg = "Sai mật khẩu", idNv = 0 }));
            }
            return JObject.FromObject((new Msg { msg = "Không tìm thấy tên đăng nhập này", idNv = 0 }));
        }
        [HttpGet]
        public tbNhanVien GettbNhanVien(int id)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbNhanViens.FirstOrDefault(x => x.id == id);
        }

        /// <returns></returns>
     

      
        public class NhanVienVM
        {
            public decimal Id { get; set; }
            public string userName { get; set; }
            public string account { get; set; }
            public string password { get; set; }
            public string position { get; set; }
            public string phone { get; set; }
            public Boolean invalid { get; set; }
        }
        public class Msg
        {
            public string msg { get; set; }
            public int idNv { get; set; }
        }
  
    }
}

