﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI_SaleCoKhi.Controllers
{
    public class CustomerController : ApiController
    {

        //get all khach hang
        [HttpGet]
        public List<KhachHangModel> getAllContomers()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbKhachHangs.Select(x => new KhachHangModel 
            {
              id=x.id,

              customerCode=x.MaKhachHang,

              customerName=x.TenKhachHang,

              address=x.DiaChi,

              phone=x.SoDienThoai,

              email=x.Email,

              nameString=x.TenKhachHangString,

              priceLevel=x.MucGia
            }).ToList();
        }

        //get customer by id
        [HttpGet]
        public List<KhachHangModel> getListCustomerById(String keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbKhachHangs.Where(p => p.TenKhachHangString.Contains(keyword) || p.DiaChiString.Contains(keyword) || p.SoDienThoai.Contains(keyword)).Select(x => new KhachHangModel 
            {
              id=x.id,

              customerCode=x.MaKhachHang,

              customerName=x.TenKhachHang,

              address=x.DiaChi,

              phone=x.SoDienThoai,

              email=x.Email,

              nameString=x.TenKhachHangString,

            }).ToList();
        }
        [HttpGet]
        public List<KhachHangModel> getListCustomerById(int page)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbKhachHangs.Select(x => new KhachHangModel
            {
                id = x.id,

                customerCode = x.MaKhachHang,

                customerName = x.TenKhachHang,

                address = x.DiaChi,

                phone = x.SoDienThoai,

                email = x.Email,

                nameString = x.TenKhachHangString,

            }).Skip((page - 1) * 100).Take(100).ToList();
        }
        [HttpGet]
        public List<KhachHangModel> getListCustomerById(int page,String keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbKhachHangs.Where(p => p.TenKhachHangString.Contains(keyword) || p.DiaChiString.Contains(keyword) || p.SoDienThoai.Contains(keyword)).Select(x => new KhachHangModel
            {
                id = x.id,

                customerCode = x.MaKhachHang,

                customerName = x.TenKhachHang,

                address = x.DiaChi,

                phone = x.SoDienThoai,

                email = x.Email,

                nameString = x.TenKhachHangString,

            }).Skip((page - 1) * 100).Take(100).ToList();
        }

        [HttpGet]
        public KhachHangModel getCustomerById(int id)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbKhachHangs.Where(p=>p.id==id).Select(x => new KhachHangModel
            {
                id = x.id,

                customerCode = x.MaKhachHang,

                customerName = x.TenKhachHang,

                address = x.DiaChi,

                phone = x.SoDienThoai,

                email = x.Email,

                nameString = x.TenKhachHangString,

            }).FirstOrDefault();
        }
        //add customer
        [HttpPost]
        public bool InsertCustomer([FromBody]JObject cus)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                tbKhachHang _cus = cus.ToObject<tbKhachHang>();

                _cus.NgayNhap = DateTime.Now;
                _cus.MaKhachHang=getIdCustomer();
                db.tbKhachHangs.InsertOnSubmit(_cus);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        [NonAction]
        public string getIdCustomer()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            tbKhachHang cus = db.tbKhachHangs.OrderByDescending(p => p.MaKhachHang).First();
            int id = int.Parse(cus.MaKhachHang)+1;
            String maKhachHang =id.ToString();
            int plus = cus.MaKhachHang.Length - maKhachHang.Length;
            for (int i = 0; i < plus; i++)
            {
                maKhachHang = "0" + maKhachHang;
            }

                return maKhachHang;
        }
        //[HttpPost]
        //public bool insertCustomer(string name, string diachi, string sdt)
        //{
        //    try
        //    {
        //        DataStoreDataContext db = new DataStoreDataContext();
        //        tbKhachHang khachhang = new tbKhachHang();
        //        khachhang.TenKhachHang = name;
        //        khachhang.DiaChi = diachi;
        //        khachhang.SoDienThoai = sdt;
        //        db.tbKhachHangs.InsertOnSubmit(khachhang);
        //        db.SubmitChanges(); //confirm change
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        // update customer
        [HttpPut]
        public bool UpdatetbCustomer([FromBody]JObject jcus)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                tbKhachHang cus = jcus.ToObject<tbKhachHang>();
                //get customer by id
                tbKhachHang kh = db.tbKhachHangs.FirstOrDefault(x => x.id == cus.id);
                if (kh == null) return false;//if not exist
                kh.TenKhachHang = cus.TenKhachHang;
                kh.DiaChi = cus.DiaChi;
                kh.SoDienThoai = cus.SoDienThoai;
                kh.Email = cus.Email;
                kh.SoFAX = cus.SoFAX;
                kh.MaSoThue = cus.MaSoThue;
                db.SubmitChanges();//confirm
                return true;
            }
            catch
            {
                return false;
            }
        }

        [HttpDelete]
        public bool DeletetbKhachHang(int id)
        {
            try
            {

                DataStoreDataContext db = new DataStoreDataContext();
                // get customer by id exist
                tbKhachHang kh = db.tbKhachHangs.FirstOrDefault(x => x.id == id);
                if (kh == null) return false;
                db.tbKhachHangs.DeleteOnSubmit(kh);
                db.SubmitChanges();//confirm change
                return true;
            }
            catch
            {
                return false;
            }
        }
        public class KhachHangModel
        {
            public int id;

            public string customerCode;

            public string customerName;

            public string address;

            public string phone;

            public string email;

            public string nameString;

            public int priceLevel;
        }

    }
}
