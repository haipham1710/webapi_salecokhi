﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI_SaleCoKhi.Controllers
{
    public class BillDetailController : ApiController
    {
        //get all chi tiet hoa don
        [HttpGet]
        public List<ChiTietHoaDonModel> getAllBillDetailLists()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbChiTietHoaDons.Select(p => new ChiTietHoaDonModel
            {
                id = p.id,
                billId = p.tbHoaDon.id,
                quantitive = p.SoLuong,
                price = p.DonGia,
                productId = p.tbSanPham.id,
                importPrice = p.GiaVon,
                discount = p.ChietKhau,
                note = p.GhiChu,
                sale = p.GiamGia,
            }).ToList();
        }
        [HttpGet]
        public List<ChiTietHoaDonModel> getAllBillDetailByIdBillLists(String id)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbChiTietHoaDons.Select(p => new ChiTietHoaDonModel
            {
                id = p.id,
                billId = p.tbHoaDon.id,
                quantitive = p.SoLuong,
                price = p.DonGia,
                productId = p.tbSanPham.id,
                productCode=p.tbSanPham.MaSanPham,
                productName=p.tbSanPham.TenSanPham,
                importPrice = p.GiaVon,
                discount = p.ChietKhau,
                note = p.GhiChu,
                sale = p.GiamGia,
            }).Where(p=>p.billId==int.Parse(id)).ToList();
        }
        //add BillDetail
        [HttpPost]
        public bool insertBillDetail([FromBody]tbChiTietHoaDon billDetail)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                db.tbChiTietHoaDons.InsertOnSubmit(billDetail);
                db.SubmitChanges(); //confirm change
                return true;
            }
            catch
            {
                return false;
            }
        }
            
    }

    public class ChiTietHoaDonModel
    {
        public int id { get; set; }

        public decimal billId { get; set; }

        public double quantitive { get; set; }

        public double price { get; set; }

        public int productId { get; set; }
        public String productCode { get; set; }
        public String productName { get; set; }

        public double importPrice { get; set; }

        public double discount { get; set; }

        public string note { get; set; }

        public decimal sale { get; set; }

    }

}
