﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI_SaleCoKhi;

namespace WebAPI_SaleCoKhi.Controllers
{
    public class OrderDetailController : ApiController
    {
        [HttpGet]
        public List<ChiTietKhachHangDatHangVM> getAllBillDetailByIdBillLists(String id)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbChiTietKhachHangDatHangs.Select(p => new ChiTietKhachHangDatHangVM
            {
                id = p.id,
                orderId=p.idKhachHangDatHang,
                quantitive = p.SoLuong,
                productID = p.tbSanPham.id,
                productName = p.tbSanPham.TenSanPham,
                productCode = p.tbSanPham.MaSanPham,
                unit = p.tbSanPham.DonVi,
                donGia = p.DonGia,
                price = p.DonGia,
                price1=p.DonGia,
                price2=p.DonGia,
                discount = p.ChietKhau,
                note = p.GhiChu,
            }).Where(p => p.orderId == int.Parse(id)).ToList();
        }
    }
    public class ChiTietKhachHangDatHangVM
    {
        public int id{get;set;}

        public int orderId{get;set;}

        public double quantitive{get;set;}

        public string unit { get; set; }
        public string productCode { get; set; }
        public int productID{get;set;}
        public string productName { get; set; }

        public double discount{get;set;}

        public string note{get;set;}
        public int donGia { get; set; }
        public double price { get; set; }
        public double price1 { get; set; }

        public double price2 { get; set; }
    }
}
