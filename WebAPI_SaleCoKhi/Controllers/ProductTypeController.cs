﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI_SaleCoKhi.Controllers
{
    public class ProductTypeController : ApiController
    {
        [HttpGet]
        public IEnumerable<LoaiSanPhamVM> GettbLoaiSanPhamLists()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbLoaiSanPhams.Select(x => new LoaiSanPhamVM
            {
                id = x.id,
                productTypeName=x.TenLoaiSanPham
            }).OrderBy(p=>p.productTypeName).AsEnumerable();
        }
        public class LoaiSanPhamVM
        {
            public int id { get; set; }

            public string productTypeName { get; set; }
        }
    }
}
