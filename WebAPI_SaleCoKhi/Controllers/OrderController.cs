﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI_SaleCoKhi;
using WebAPI_SaleCoKhi.Models;

namespace WebAPI_SaleCoKhi.Controllers
{
    public class OrderController : ApiController
    {
        [HttpGet]
        public List<KhachHangDatHangVM> GettbKhachHangDatHangLists()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwKhachHangDatHangs.Where(p=>p.DaLapHoaDon==false).Select(p => new KhachHangDatHangVM
            {
                orderNumber = p.SoPhieu,
                id = (int)p.id,
                dateCreated = p.NgayLap,
                deliveryDate = p.NgayDuKienGiaoHang,
                customerName = p.TenKhachHang,
                userName = p.TenNhanVien,
                customerId = p.idKhachHang,
                userId = p.idNhanVien,
                address = p.DiaChi,
                phone = p.SoDienThoai,
                isCreateBill = p.DaLapHoaDon,
                discount = p.ChietKhau,
                vat = p.VAT,
                amount=p.TongTien,
                note = p.GhiChu,
                billType=p.LoaiHoaDon
            }).OrderByDescending(q => q.id).ToList();
        }
        [HttpGet]
        public List<KhachHangDatHangVM> GettbKhachHangDatHangLists(string keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwKhachHangDatHangs.Where(p => p.TenKhachHangString.Contains(keyword) && p.DaLapHoaDon == false).Select(p => new KhachHangDatHangVM
            {
                orderNumber = p.SoPhieu,
                id = (int)p.id,
                dateCreated = p.NgayLap,
                deliveryDate = p.NgayDuKienGiaoHang,
                customerName = p.TenKhachHang,
                userName = p.TenNhanVien,
                customerId = p.idKhachHang,
                userId = p.idNhanVien,
                address = p.DiaChi,
                phone = p.SoDienThoai,
                isCreateBill = p.DaLapHoaDon,
                discount = p.ChietKhau,
                vat = p.VAT,
                amount = p.TongTien,
                note = p.GhiChu,
                billType = p.LoaiHoaDon
            }).OrderByDescending(q => q.id).ToList();
        }
        [HttpGet]
        public List<KhachHangDatHangVM> GettbHoaDonLists(int page, int idEmployee, int idCustomer, String dateFrom, String dateTo)
        {
            DateTime dtDateFrom;
            DateTime dtDateTo;
            if (!dateFrom.Equals("null"))
            {
                dtDateFrom = DateTime.ParseExact(dateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else
                dtDateFrom = DateTime.MinValue;
            if (!dateTo.Equals("null"))
            {
                dtDateTo = DateTime.ParseExact(dateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else
                dtDateTo = DateTime.Now;
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwKhachHangDatHangs.Where(x => x.DaLapHoaDon==false
                                            && (idEmployee == 0 || x.idNhanVien == idEmployee)
                                            && (idCustomer == 0 || x.idKhachHang == idCustomer)
                                            && (dtDateFrom == DateTime.MinValue || x.NgayLap >= dtDateFrom)
                                            && (dtDateTo == DateTime.Now || x.NgayLap <= dtDateTo)).Select(p => new KhachHangDatHangVM
                                            {
                                                orderNumber = p.SoPhieu,
                                                id = (int)p.id,
                                                dateCreated = p.NgayLap,
                                                deliveryDate = p.NgayDuKienGiaoHang,
                                                customerName = p.TenKhachHang,
                                                userName = p.TenNhanVien,
                                                customerId = p.idKhachHang,
                                                userId = p.idNhanVien,
                                                address = p.DiaChi,
                                                phone = p.SoDienThoai,
                                                isCreateBill = p.DaLapHoaDon,
                                                discount = p.ChietKhau,
                                                vat = p.VAT,
                                                amount = p.TongTien,
                                                note = p.GhiChu,
                                                billType = p.LoaiHoaDon
                                            }).OrderByDescending(q => q.id).Skip((page - 1) * 100).Take(100).ToList();
        }
        [HttpGet]
        public List<KhachHangDatHangVM> GettbHoaDon(int page, String keyword, int idEmployee, int idCustomer, String dateFrom, String dateTo)
        {
            DateTime dtDateFrom;
            DateTime dtDateTo;
            if (!dateFrom.Equals("null"))
            {
                dtDateFrom = DateTime.ParseExact(dateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else
                dtDateFrom = DateTime.MinValue;
            if (!dateTo.Equals("null"))
            {
                dtDateTo = DateTime.ParseExact(dateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else
                dtDateTo = DateTime.Now;
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwKhachHangDatHangs.Where(x => x.DaLapHoaDon == false
                                                              && (String.IsNullOrEmpty(keyword) || x.TenKhachHangString.Contains(keyword) || x.TenNhanVien.Contains(keyword))
                                                              && (idEmployee == 0 || x.idNhanVien == idEmployee)
                                                              && (idCustomer == 0 || x.idKhachHang == idCustomer)
                                                              && (dtDateFrom == DateTime.MinValue || x.NgayLap >= dtDateFrom)
                                                              && (dtDateTo == DateTime.Now || x.NgayLap <= dtDateTo)).Select(p => new KhachHangDatHangVM
                                                              {
                                                                  orderNumber = p.SoPhieu,
                                                                  id = (int)p.id,
                                                                  dateCreated = p.NgayLap,
                                                                  deliveryDate = p.NgayDuKienGiaoHang,
                                                                  customerName = p.TenKhachHang,
                                                                  userName = p.TenNhanVien,
                                                                  customerId = p.idKhachHang,
                                                                  userId = p.idNhanVien,
                                                                  address = p.DiaChi,
                                                                  phone = p.SoDienThoai,
                                                                  isCreateBill = p.DaLapHoaDon,
                                                                  discount = p.ChietKhau,
                                                                  vat = p.VAT,
                                                                  amount = p.TongTien,
                                                                  note = p.GhiChu,
                                                                  billType = p.LoaiHoaDon
                                                              }).OrderByDescending(q => q.id).Skip((page - 1) * 100).Take(100).ToList();
        }
        [NonAction]
        public bool ChangeOrderStatus(int id)
        {
            try {
                DataStoreDataContext db = new DataStoreDataContext();
                db.tbKhachHangDatHangs.Where(p => p.id == id).FirstOrDefault().DaLapHoaDon = true;
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        [HttpPost]
        public bool InsertOrder([FromBody]tbKhachHangDatHang order)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                order.NgayLap = DateTime.Now;
                order.NgayDuKienGiaoHang = DateTime.Now;
                order.DaLapHoaDon = false;
                order.SoPhieu = getSoPhieu(order);

                db.tbKhachHangDatHangs.InsertOnSubmit(order);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        //[HttpPost]
        //public ResultModel InsertOrder(tbKhachHangDatHang order)
        //{
        //    try
        //    {
        //        DataStoreDataContext db = new DataStoreDataContext();
        //        order.NgayLap = DateTime.Now;
        //        order.NgayDuKienGiaoHang = DateTime.Now;
        //        order.DaLapHoaDon = false;
        //        order.SoPhieu = getSoPhieu(order);
        //        db.tbKhachHangDatHangs.InsertOnSubmit(order);
        //        db.SubmitChanges();
        //        return ResultModel.MakeSucess("Thêm đặt hàng thành công");
        //    }
        //    catch
        //    {
        //        return ResultModel.MakeError("Thêm đặt hàng thất bại");
        //    }
        //}
        [HttpPut]
        public bool EditOrder([FromBody]tbKhachHangDatHang order)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                
                tbKhachHangDatHang oldOrder= db.tbKhachHangDatHangs.Where(p => p.id == order.id).FirstOrDefault();
                oldOrder.tbChiTietKhachHangDatHangs = order.tbChiTietKhachHangDatHangs;
                oldOrder.ChietKhau = order.ChietKhau;
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        [NonAction]
        public int getSoPhieu(tbKhachHangDatHang order)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            tbKhachHangDatHang index = db.tbKhachHangDatHangs.Where(x => x.NgayLap.Month == order.NgayLap.Month && x.NgayLap.Year == order.NgayLap.Year).OrderByDescending(p => p.id).FirstOrDefault();
            if (index == null)
                return 1;
            else
                return index.SoPhieu + 1;
        }
        
    }
    public class KhachHangDatHangVM
    {
        public int orderNumber{get;set;}

        public int id{get;set;}

        public System.DateTime dateCreated{get;set;}

        public System.DateTime deliveryDate{get;set;}

        public int customerId{get;set;}
        public int billType { get; set; }
        public int userId{get;set;}

        public double discount{get;set;}

        public int vat{get;set;}
        public double amount { get; set; }
        public string note{get;set;}

        public string userName{get;set;}

        public string customerCode{get;set;}

        public string customerCodeString{get;set;}

        public string customerName{get;set;}

        public string customerNameString{get;set;}

        public string address{get;set;}

        public string addressString{get;set;}

        public string phone{get;set;}

        public bool isCreateBill{get;set;}
    }
}
