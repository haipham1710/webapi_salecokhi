﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Web.Http;
using WebAPI_SaleCoKhi.Controllers;
using WebAPI_SaleCoKhi.Models;

namespace WebAPI_SaleCoKhi.Controllers
{
    public class BillController : ApiController
    {

        [HttpGet]
        public List<HoaDonModel> GettbHoaDonLists(int page, int idEmployee, int idCustomer, String dateFrom, String dateTo)
        {

           
            DateTime dtDateFrom;
            DateTime dtDateTo;
            if (!dateFrom.Equals("null"))
            {
                dtDateFrom = DateTime.ParseExact(dateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else
                dtDateFrom = DateTime.MinValue;
            if (!dateTo.Equals("null"))
            {
                dtDateTo = DateTime.ParseExact(dateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else
                dtDateTo = DateTime.Now;
            DataStoreDataContext db = new DataStoreDataContext();
            return db.vwHoaDons.Where(x => (idEmployee == 0 || x.idNhanVien == idEmployee)
                                            &&(idCustomer==0||x.idKhachHang==idCustomer)
                                            && (dtDateFrom == DateTime.MinValue || x.NgayLap >= dtDateFrom)
                                            && (dtDateTo == DateTime.Now || x.NgayLap <= dtDateTo)).Select(p => new HoaDonModel
            {
                billCode=p.MaHoaDon,
                id = (int)p.id,
                dateCreated = p.NgayLap,
                customerName = p.TenKhachHang,
                userName = p.TenNhanVien,
                customerId = p.idKhachHang,
                userId = p.idNhanVien,
                discount = p.ChietKhau,
                billNumber = p.SoHoaDon,
                vat = p.VAT,
                note = p.GhiChu,
                customerPay = (Int32)p.SoTienKhachDua,
                billType = p.LoaiHoaDon,
                payment = p.HinhThucThanhToan,
                amount = p.TongTien
            }).OrderByDescending(q => q.id).Skip((page - 1) * 100).Take(100).ToList();
        }



        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public List<HoaDonModel> GettbHoaDon(int page,String keyword,int idEmployee,int idCustomer,String dateFrom,String dateTo)
        {
            DateTime dtDateFrom;
            DateTime dtDateTo;
            if (!dateFrom.Equals("null"))
            {
                dtDateFrom = DateTime.ParseExact(dateFrom, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else
                dtDateFrom = DateTime.MinValue;
            if (!dateTo.Equals("null"))
            {
                dtDateTo = DateTime.ParseExact(dateTo, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            else
                dtDateTo = DateTime.Now;
            DataStoreDataContext db = new DataStoreDataContext();
            List<HoaDonModel> hoaDonList = db.vwHoaDons.Where(x => (String.IsNullOrEmpty(keyword) || x.TenKhachHangString.Contains(keyword) ||x.TenNhanVien.Contains(keyword) )
                                                              && (idEmployee == 0 || x.idNhanVien == idEmployee)
                                                              &&(idCustomer==0||x.idKhachHang==idCustomer)
                                                              && (dtDateFrom == DateTime.MinValue || x.NgayLap >= dtDateFrom)
                                                              && (dtDateTo == DateTime.Now || x.NgayLap <= dtDateTo)).Select(p => new HoaDonModel
            {
                billCode = p.MaHoaDon,
                id = (int)p.id,
                dateCreated = p.NgayLap,
                customerName = p.TenKhachHang,
                userName = p.TenNhanVien,
                customerId = p.idKhachHang,
                userId = p.idNhanVien,
                discount = p.ChietKhau,
                billNumber = p.SoHoaDon,
                vat = p.VAT,
                note = p.GhiChu,
                customerPay = (Int32)p.SoTienKhachDua,
                billType = p.LoaiHoaDon,
                payment = p.HinhThucThanhToan,
                amount = p.TongTien
            }).OrderByDescending(q => q.id).Skip((page - 1) * 100).Take(100).ToList();
            return hoaDonList;
        }
       
        internal decimal getIdBill(BillController bill)
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public ResultModel insertBill(tbHoaDon bill)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                if (bill.id != 0)
                {
                    OrderController khachhangdathang = new OrderController();
                    khachhangdathang.ChangeOrderStatus((int)bill.id);
                    bill.idKhachHangDatHang = (int)bill.id;
                }
                bill.NgayLap = DateTime.Now;
                bill.SoHoaDon = getSoHoaDon(bill);

                //Thêm vào tb Thu nợ khách hàng
                bill.tbThuNoKhachHangs.FirstOrDefault().NgayThu = DateTime.Now;
                bill.tbThuNoKhachHangs.FirstOrDefault().SoPhieu = getSoPhieu(bill.tbThuNoKhachHangs.FirstOrDefault());

                //Trừ số lượng trong kho
                foreach (tbChiTietHoaDon billDetail in bill.tbChiTietHoaDons)
                {
                    db.tbSanPhams.Where(p => p.id == billDetail.idSanPham).FirstOrDefault().SoLuong -= billDetail.SoLuong;
                }
                db.tbHoaDons.InsertOnSubmit(bill);
                db.SubmitChanges(); //confirm change

                return ResultModel.MakeSucess("Thêm hóa đơn thành công",(int)bill.id);
            }
            catch(Exception e)
            {
                return ResultModel.MakeError("Thêm hóa đơn thất bại. \nLỗi: "+e.Message);
            }
        }
        [NonAction]
        public int getSoPhieu(tbThuNoKhachHang tbThuNoKhachHang)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            tbThuNoKhachHang index = db.tbThuNoKhachHangs.Where(x => x.NgayThu.Month == tbThuNoKhachHang.NgayThu.Month && x.NgayThu.Year == tbThuNoKhachHang.NgayThu.Year).OrderByDescending(p => p.id).FirstOrDefault();
            if (index == null)
                return 1;
            else
                return index.SoPhieu + 1;
        }
       
        [NonAction]
        public int getSoHoaDon(tbHoaDon bill)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            tbHoaDon product = db.tbHoaDons.Where(x=>x.NgayLap.Month == bill.NgayLap.Month && x.NgayLap.Year == bill.NgayLap.Year).OrderByDescending(p => p.id).FirstOrDefault();
            //int id = 1;
            if (product == null)
                return 1;
            else
                return product.SoHoaDon + 1;
            
        }
        // update bill
        [HttpPut]
        public bool UpdatetbBill([FromBody]JObject jsonBill)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                tbHoaDon billUpdate = jsonBill.ToObject<tbHoaDon>();
                //get customer by id
                tbHoaDon bill = db.tbHoaDons.FirstOrDefault(x => x.id == billUpdate.id);
                if (bill == null) return false;//if not exist
                bill.NgayLap = DateTime.Now;
                bill.idKhachHang = billUpdate.idKhachHang;
                bill.idNhanVien = billUpdate.idNhanVien;
                bill.LoaiHoaDon = billUpdate.LoaiHoaDon;
                bill.SoHoaDon = billUpdate.SoHoaDon;
                bill.VAT = billUpdate.VAT;
                bill.HinhThucThanhToan = billUpdate.HinhThucThanhToan;
                bill.ChietKhau = billUpdate.ChietKhau;
                db.SubmitChanges();//confirm
                return true;
            }
            catch
            {
                return false;
            }
        }

        //delete bill
        [HttpDelete]
        public bool DeletetbHoaDon(int id)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                //lấy HoaDon tồn tại ra
                tbHoaDon hd = db.tbHoaDons.FirstOrDefault(x => x.id == id);
                if (hd == null) return false;
                db.tbHoaDons.DeleteOnSubmit(hd);
                db.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        

    }

    public class HoaDonModel
    {
        public String billCode { get; set; }
        public int id { get; set; }

        public System.DateTime dateCreated { get; set; }

        public String customerName { get; set; }

        public int userId { get; set; }

        public int customerId { get; set; }

        public String userName { get; set; }

        public double discount { get; set; }

        public int billNumber { get; set; }

        public int vat { get; set; }

        public string note { get; set; }

        public Int32 customerPay { get; set; }

        public int billType { get; set; }

        public string payment { get; set; }
        public double amount { get; set; }
    }
}
