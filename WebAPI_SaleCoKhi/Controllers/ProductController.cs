﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI_SaleCoKhi.Controllers
{
    public class ProductController : ApiController
    {
        //get all san pham
        [HttpGet]
        public IEnumerable<SanPhamVM> getAllProducts()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbSanPhams.Where(p=>p.HetBan==false).Select(x => new SanPhamVM
                {
                    id = x.id,
                    productCode=x.MaSanPham,
                    productName=x.TenSanPham,
                    unit=x.DonVi,
                    price=x.GiaBan,
                    quantitive=x.SoLuong,
                    productTypeId=x.idLoaiSanPham,
                    description=x.MoTa,
                    dateCreated=x.NgayTao,
                    price1=x.GiaSiCapMot,
                    price2=x.GiaSiCapHai
                }).AsEnumerable();
        }

        //get Product by id
        [HttpGet]
        public IEnumerable<SanPhamVM> getAllProducts(string keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbSanPhams.Where(p => p.HetBan == false && (p.TenSanPhamString.Contains(keyword) || p.MaSanPham.Contains(keyword))).Select(x => new SanPhamVM
            {
                id = x.id,
                productCode = x.MaSanPham,
                productName = x.TenSanPham,
                unit = x.DonVi,
                price = x.GiaBan,
                quantitive = x.SoLuong,
                productTypeId = x.idLoaiSanPham,
                description = x.MoTa,
                dateCreated = x.NgayTao,
                price1 = x.GiaSiCapMot,
                price2 = x.GiaSiCapHai
            }).AsEnumerable();
        }
        [HttpGet]
        public List<SanPhamVM> getAllProducts(int page, int idlsp)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbSanPhams.Where(p => p.idLoaiSanPham == idlsp && p.HetBan == false).Select(x => new SanPhamVM
            {
                id = x.id,
                productCode = x.MaSanPham,
                productName = x.TenSanPham,
                unit = x.DonVi,
                price = x.GiaBan,
                quantitive = x.SoLuong,
                productTypeId = x.idLoaiSanPham,
                description = x.MoTa,
                dateCreated = x.NgayTao,
                price1 = x.GiaSiCapMot,
                price2 = x.GiaSiCapHai
            }).ToList().Skip((page - 1) * 100).Take(100).ToList();
        }
        [HttpGet]
        public List<SanPhamVM> getAllProducts(int page,int idlsp,string keyword)
        {
            DataStoreDataContext db = new DataStoreDataContext();
            return db.tbSanPhams.Where(p => p.idLoaiSanPham == idlsp && p.HetBan == false && (p.TenSanPhamString.Contains(keyword) || p.MaSanPham.Contains(keyword))).Select(x => new SanPhamVM
            {
                id = x.id,
                productCode = x.MaSanPham,
                productName = x.TenSanPham,
                unit = x.DonVi,
                price = x.GiaBan,
                quantitive = x.SoLuong,
                productTypeId = x.idLoaiSanPham,
                description = x.MoTa,
                dateCreated = x.NgayTao,
                price1 = x.GiaSiCapMot,
                price2 = x.GiaSiCapHai
            }).ToList().Skip((page - 1) * 100).Take(100).ToList();
        }

        //add Product
        [HttpPost]
        public bool insertProduct([FromBody]JObject jsp)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                tbSanPham sp = jsp.ToObject<tbSanPham>();
                sp.NgayTao = DateTime.Now;
                sp.MaSanPham = getIdProduct();
                db.tbSanPhams.InsertOnSubmit(sp);
                db.SubmitChanges(); //confirm change
                return true;
            }
            catch
            {
                return false;
            }
        }
        [NonAction]
        public string getIdProduct()
        {
            DataStoreDataContext db = new DataStoreDataContext();
            tbSanPham product = db.tbSanPhams.OrderByDescending(p => p.MaSanPham).First();
            int id = int.Parse(product.MaSanPham) + 1;
            String maSanPham = id.ToString();
            int plus = product.MaSanPham.Length - maSanPham.Length;
            for (int i = 0; i < plus; i++)
            {
                maSanPham = "0" + maSanPham;
            }

            return maSanPham;
        }
        // update Product
        [HttpPut]
        public bool UpdatetbNhanVien([FromBody]JObject jsp)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                tbSanPham _sp = jsp.ToObject<tbSanPham>();
                //get Product by id
                tbSanPham sp = db.tbSanPhams.FirstOrDefault(x => x.id == _sp.id);
                if (sp == null) return false;//if not exist
                sp.TenSanPham = _sp.TenSanPham;
                sp.idLoaiSanPham = _sp.idLoaiSanPham;
                sp.GiaSiCapMot = _sp.GiaSiCapMot;
                sp.GiaBan = _sp.GiaBan;
                sp.SoLuong = _sp.SoLuong;
                sp.MoTa = _sp.MoTa;
                sp.DonVi = _sp.DonVi;
                db.SubmitChanges();//confirm
                return true;
            }
            catch
            {
                return false;
            }
        }

        [HttpDelete]
        public bool DeletetbNhanVien(int id)
        {
            try
            {
                DataStoreDataContext db = new DataStoreDataContext();
                // get Product by id exist
                tbSanPham sp = db.tbSanPhams.FirstOrDefault(x => x.id == id);
                if (sp == null) return false;
                db.tbSanPhams.DeleteOnSubmit(sp);
                db.SubmitChanges();//confirm change
                return true;
            }
            catch
            {
                return false;
            }
        }
        public class SanPhamVM
        {
            public int id { get; set; }

            public string productCode { get; set; }

            public string productName { get; set; }

            public string unit { get; set; }

            public double price { get; set; }

            public double quantitive { get; set; }

            public int productTypeId { get; set; }

            public string description { get; set; }

            public System.DateTime dateCreated { get; set; }

            public double price1 { get; set; }

            public double price2 { get; set; }
        }
    }
}
